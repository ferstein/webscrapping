# -*- coding: utf-8 -*-
#comandtorun scrapy runspider consisa_scrapy2.py -o output.csv
import scrapy
from scrapy.http import FormRequest
import csv

class Consisa(scrapy.Spider):
	name = 'consisa'
	a = csv.DictReader(open('Test.csv'))
	# handle_httpstatus_list = [404, 403, 503, 500]
	start_urls = ['https://consisa.com.mx/nss']
	# month_dict = {'Enero': '01', 'Febrero': '02', 'Marzo': '03', 'Abril': '04', 'Mayo': '05',
	# 	'Junio': '06', 'Julio': '07', 'Agosto': '08', 'Septiembre': '09', 'Octubre': '10',
	# 	'Noviembre': '11', 'Diciembre': '12'}
	month_dict = {'jan': '01', 'feb': '02', 'mar': '03', 'apr': '04', 'may': '05',
		'jun': '06', 'jul': '07', 'aug': '08', 'sep': '09', 'oct': '10',
		'nov': '11', 'dec': '12'}

	custom_settings = {
        # 'ROBOTSTXT_OBEY': True,
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0',
        # 'COOKIES_ENABLED': False,
        # 'DOWNLOADER_MIDDLEWARES': {},
        # 'AUTOTHROTTLE_ENABLED': True,
        # 'AUTOTHROTTLE_START_DELAY': 5,
        # 'AUTOTHROTTLE_MAX_DELAY': 60,
        # 'AUTOTHROTTLE_TARGET_CONCURRENCY': 0.5,
        # 'DOWNLOAD_DELAY': 0.4,
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 1,}
	}


	def parse(self, response):

		url = 'https://consisa.com.mx/nss'
		for row in self.a:
			date_ = str(row['fechadenacimiento'])
			Dia = date_[:2]
			Mes = date_[2:5]
			Anio = date_[5:]
			if Mes:
				month = self.month_dict[Mes]

				data = {'strNombre': row['nombre'], 'strPrimerApellido': row['primerapellido'],
					'strSegundoApellido': row['segundoapellido'], 'strdia': Dia,
					'strmes': month, 'stranio': Anio, 'task': 'consultar.afiliacionImss'}
				request = FormRequest(url, formdata=data, callback=self.parse_first)
				yield request

	def parse_first(self, response):
		item = {}
		item['Nombre'] = response.xpath('//td[strong/text()="Nombre:"]/following-sibling::td//text()').extract_first().strip()
		item['Fecha de nacimiento'] = response.xpath('//td[strong/text()="Fecha de nacimiento"]/following-sibling::td//text()').extract_first().strip()
		item['RFC'] = response.xpath('//td[strong/text()="RFC:"]/following-sibling::td//text()').extract_first().strip()	
		item['NSS'] = response.xpath('//td[strong/text()="NSS:"]/following-sibling::td//text()').extract_first().strip()
		yield item